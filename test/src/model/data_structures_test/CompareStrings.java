package model.data_structures_test;

public class CompareStrings implements Comparable{
	
	@Override
	public int compareTo(Object arg0) {

		if (this.compareTo(arg0) < 0){
			return -1;
		}
		else if(this.compareTo(arg0) > 0){
			return 1;
		}
		return 0;
	}
}
