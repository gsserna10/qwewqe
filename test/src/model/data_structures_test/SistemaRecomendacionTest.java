package model.data_structures_test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;
import model.logic.SistemaRecomendacionPeliculas;

public class SistemaRecomendacionTest extends TestCase {
	SistemaRecomendacionPeliculas sistema;
	ListaEncadenada<String> lista;

	private void setUpEscenario1() {
		sistema = new SistemaRecomendacionPeliculas();
		lista = new ListaEncadenada<>();
	}

	public void testCargarPeliculasSR() {
		setUpEscenario1();
		assertTrue("Las peliculas no se cargaron correctamente",
				sistema.cargarPeliculasSR("data/ml-latest-small/movies.csv"));
	}

	public void testCargarRatingsSR() {
		setUpEscenario1();
		assertTrue("Los ratings no se cargaron correctamente",
				sistema.cargarRatingsSR("data/ml-latest-small/ratings.csv"));

	}

	public void testCargarTagsSR() {
		setUpEscenario1();
		assertTrue("Los tags no se cargaron correctamente",
				sistema.cargarTagsSR("data/ml-latest-small/tags.csv"));
	}

	public void testSizeMoviesSR() {
		
	}

	public void testSizeUsersSR() {

	}

	public void testSizeTagsSR() {

	}

	public void testPeliculasPopularesSR() {
		
	}

	public void testCatalogoPeliculasOrdenadoSR() {

	}

	public void testRecomendarGeneroSR() {

	}

	public void testOpinionRatingsGeneroSR() {

	}

	public void testRecomendarPeliculasSR() {

	}

	public void testRatingsPeliculaSR() {

	}

	public void testUsuariosActivosSR() {

	}

	public void testCatalogoUsuariosOrdenadoSR() {

	}

	public void testRecomendarTagsGeneroSR() {

	}

	public void testOpinionTagsGeneroSR() {

	}

	public void testRecomendarUsuariosSR() {

	}

	public void testTagsPeliculaSR() {

	}

	public void testAgregarOperacionSR() {

	}

	public void testDarHistoralOperacionesSR() {

	}

	public void trestLimpiarHistorialOperacionesSR() {

	}

	public void testDarUltimasOperaciones() {

	}

	public void testBorrarUltimasOperaciones() {

	}

	public void testAgregarPelicula() {

	}

	public void testAgregarRating() {

	}

}