package model.data_structures_test;

import model.data_structures.DoubleLinkedList;
import junit.framework.TestCase;
import junit.framework.TestCase;

public class DoubleLinkedListTest extends TestCase{
	
	/*------------------------------------------------------------*/
	/*							  Lista		                      */
	/*------------------------------------------------------------*/

	
	
	/**
	 * Lista doble a probar
	 */
	private DoubleLinkedList<String> listaDoble;
	
	
	/*------------------------------------------------------------*/
	/*						Escenarios            		          */
	/*------------------------------------------------------------*/
	
	/**
	 * Escenario vacio.
	 */
	public void setupEscenario1(){
		listaDoble = new DoubleLinkedList<String>();
	}
	
	
	/**
	 * Escenario con elementos
	 */
	public void setupEscenario2(){
		
		listaDoble = new DoubleLinkedList<String>();
		listaDoble.add("Hola");
		listaDoble.add("Soy");
		listaDoble.add("Un");
		listaDoble.add("Estudiante");
		
	}
	
	/*------------------------------------------------------------*/
	/*							Pruebas		       		          */
	/*------------------------------------------------------------*/
	
	/**
	 * Prueba el metodo agregarElementoFinal en la lista doble.
	 */
	public void testAdd(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vacio*/
		/*----------------------------------------------------------------*/
		
		setupEscenario1();
		listaDoble.add("Queens");
		assertEquals("Deberia ser Queen","Queens",listaDoble.get(0));


		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		listaDoble.add("Queens");
		assertEquals("Deberia ser Queens","Queens",listaDoble.get(4));
	}
	
	/**
	 * Prueba el metodo agregarElementoFinal en la lista doble.
	 */
	public void testAddFirst(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vacio*/
		/*----------------------------------------------------------------*/
		
		setupEscenario1();
		listaDoble.addFirst("Queens");
		assertEquals("Deberia ser Queen","Queens",listaDoble.get(0));


		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		listaDoble.addFirst("Queens");
		assertEquals("Deberia ser Queen","Queens",listaDoble.get(0));
	}

	/**
	 * Prueba el metodo darElemento en la lista doble.
	 */
	public void testGet(){

		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vacio*/
		/*----------------------------------------------------------------*/
		setupEscenario1();

		boolean ex = false;
		try{
			listaDoble.get(0);
		}catch(Exception e){
			ex = true;
		}
		assertTrue("Deberia lanzar exception",ex);

		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		assertEquals("Deberia ser Un.","Un",listaDoble.get(2));
	}
	
	/**
	 * Prueba el metodo darElemento en la lista doble.
	 */
	public void testRemove(){

		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vacio*/
		/*----------------------------------------------------------------*/
		setupEscenario1();

		boolean ex = false;
		try{
			listaDoble.remove(5);
		}catch(Exception e){
			ex = true;
		}
		assertTrue("Deberia lanzar exception",ex);

		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		assertEquals("Deberia ser Un.","Un",listaDoble.remove(2));
		assertEquals("Deberia ser Estudiante.","Estudiante",listaDoble.get(2));
	}
	
	/**
	 * Prueba el metodo darElemento en la lista doble.
	 */
	public void testRemoveFirst(){
		setupEscenario2();
		assertEquals("Deberia ser Hola.","Hola",listaDoble.removeFirst());
		assertEquals("Deberia ser Soy.","Soy",listaDoble.get(0));
	}
	
	/**
	 * Prueba el metodo darElemento en la lista doble.
	 */
	public void testRemoveLast(){
		setupEscenario2();
		assertEquals("Deberia ser Estudiante.","Estudiante",listaDoble.removeLast());
		assertEquals("Deberia ser Un.","Un",listaDoble.get(2));
	}
	
	/**
	 * Prueba el metodo darNumeroElementos en la lista doble.
	 */
	public void testSize(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vacio*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		assertEquals("Deberia ser cero",0,listaDoble.size());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		setupEscenario2();
		assertEquals("Deberia ser cuatro.",4,listaDoble.size());
	}

	
}