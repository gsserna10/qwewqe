package model.data_structures_test;

import java.util.Comparator;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.ILista;
import model.logic.ListMergeSort;
import model.logic.MergeSort;

public class SortTest extends TestCase {
	
	public ILista<Comparable> list;
	public ILista<Integer> expected;
	public Comparator t;
	
	public Integer[] lista;
	public Integer[] expect;

	public void setupEscenario1(){
		list = new DoubleLinkedList<Comparable>();
		expected = new DoubleLinkedList<Integer>();
		t = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				int y = o1-o2; 
				if(y<0)
					return -1;
				else if(y>0)
					return 1;
				return 0;
			}
		};
		for (int i = 1; i <= 1000; i++) {
			list.add(1000-i);
		}
		for (int i = 0; i < 1000; i++) {
			expected.add(i);
		}
	}
	
	public void setupEscenario2(){
		lista = new Integer[1000];
		expect = new Integer[1000];
		for (int i = 1000; i>0; i--) {
			lista[i-1] = (i-1) ;
		}
		for (int i = 0; i < 1000; i++) {
			expect[i] = i ;
		}
	}
	
	public void testSort(){
		setupEscenario2();
		MergeSort.sort(lista);
		for (int i = 0; i < lista.length; i++) {
			assertEquals(lista[i], expect[i]);
		}
	}
	public void testSort2(){
		setupEscenario1();
		ListMergeSort.sort(list,t);
		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i),expected.get(i));
		}
	}
	
}