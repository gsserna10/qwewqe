package model.data_structures_test;

import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;

public class ListaEncadenadaTest extends TestCase{
	
	// -----------------------------------------------------------------
	// Lista
	// ----------------------------------------------------------------- 
	
	/**
	 * Lista   a probar
	 */
	private ListaEncadenada<String> lista;
	
	
	// -----------------------------------------------------------------
	// Escenarios
	// -----------------------------------------------------------------
	
	/**
	 * Escenario vac�o.
	 */
	public void setupEscenario1(){
		
		/*	Se inicializa la lista  */
		lista = new ListaEncadenada<String>();
	}
	
	
	/**
	 * Escenario con elementos
	 */
	public void setupEscenario2(){

		/*	Se inicializa la lista  */
		lista = new ListaEncadenada<String>();
		for (int i = 0; i <= 1000; i++) {
			lista.add("Elemento " + i);
		}
		
	}
	
	// -----------------------------------------------------------------
	// Pruebas
	// -----------------------------------------------------------------
	
	/**
	 * Prueba el m�todo agregarElementoFinal en la lista  .
	 */
	public void testAgregarElementoFinal(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		lista.add("Queens");
		assertEquals("Deberia ser Queens","Queens",lista.get(0));
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		
		lista.add("Queens");
		assertEquals("Deberia ser Queens","Queens",lista.get(1001));
	}
	
	public void testAgregarElementInicio(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		lista.addFirst("Queens");
		assertEquals("Deberia ser Queens","Queens",lista.get(0));
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		
		setupEscenario2();
		
		lista.addFirst("Queens");
		assertEquals("Deberia ser Queens","Queens",lista.get(0));
	}
	
	public void testEliminarPrimerElemento(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		lista.removeFirst();
		assertNull(lista.removeFirst());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/		
		setupEscenario2();
		assertEquals("Deberia ser 'Elemento 0'","Elemento 0", lista.removeFirst());
		assertEquals("Deber�a ser 'Elemento 1'", "Elemento 1", lista.get(0));
	}
	
	public void testEliminarUltimoElemento(){
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		lista.removeLast();
		assertNull(lista.removeLast());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/		
		setupEscenario2();
		assertEquals("Deberia ser 'Elemento 1000'","Elemento 1000", lista.removeLast());
		assertEquals("Deber�a ser 'Elemento 999'", "Elemento 999", lista.get(999));
	}
	
	/**
	 * Prueba el m�todo darElemento en la lista  .
	 */
	public void testDarElemento(){

		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		
		setupEscenario1();
		boolean ex = false;
		try{
			assertNull(lista.get(0));
		}catch(IndexOutOfBoundsException e){
			ex = true;
		}
		assertTrue("Deberia arrojar exception",ex);

		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		setupEscenario2();
		assertEquals("Deberia ser 'Elemento 500'.","Elemento 500",lista.get(500));
	}
	
	/**
	 * Prueba el m�todo darNumeroElementos en la lista  .
	 */
	public void testDarNumeroElementos(){
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		assertEquals("Deberia ser cero",0,lista.size());
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		setupEscenario2();
		assertEquals("Deberian ser mil",1001,lista.size());
	}
	
	/**
	 * Prueba el m�todo eliminar elemento de una posici�n dada por par�metro.
	 */
	public void testEliminarElemento(){
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario vac�o*/
		/*----------------------------------------------------------------*/
		setupEscenario1();
		boolean ex = false;
		try{
			assertNull(lista.get(0));
		}catch(IndexOutOfBoundsException e){
			ex = true;
		}
		assertTrue("Deberia arrojar exception",ex);
		
		/*----------------------------------------------------------------*/
		/*Prueba con un escenario con elementos*/
		/*----------------------------------------------------------------*/
		setupEscenario2();
		assertEquals("Deber�a retornar el elemento 500","Elemento 500", lista.remove(500));
		
	}
}
