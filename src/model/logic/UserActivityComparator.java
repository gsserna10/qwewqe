package model.logic;

import java.util.Comparator;

import model.vo.VOUsuarioConteo;

public class UserActivityComparator implements Comparator<VOUsuarioConteo>{

	public int compare(VOUsuarioConteo arg0, VOUsuarioConteo arg1) {
		// TODO Auto-generated method stub
		return arg1.getConteo()-arg0.getConteo();
	}
}
