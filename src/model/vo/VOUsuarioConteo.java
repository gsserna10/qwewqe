package model.vo;

public class VOUsuarioConteo implements Comparable{
	private long idUsuario;
	private int conteo;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		if(this.compareTo(o)<0)
			return -1;
		if(this.compareTo(o)>0)
			return 1;
		return 0;
	}
}
