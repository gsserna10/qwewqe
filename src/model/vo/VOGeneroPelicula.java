package model.vo;

import model.data_structures.ILista;

public class VOGeneroPelicula {
	
	private String genero;
	
	private ILista<VOPelicula> peliculas;

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	} 

}
