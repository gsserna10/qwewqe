package model.vo;

import model.data_structures.ILista;

public class VOUsuario implements Comparable{

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;		/*	*	*/
	private ILista<String> tagsAsociados;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	public ILista<String> getTagsAsociados(){
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> a){
		this.tagsAsociados = a;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		VOUsuario actual = (VOUsuario)o;
		if(primerTimestamp<actual.primerTimestamp)
			return -1;
		else if(primerTimestamp>actual.primerTimestamp)
			return 1;
		else{
			if(numRatings<actual.numRatings)
				return -1;
			else if(numRatings>actual.numRatings)
				return 1;
			else{
				if(idUsuario<actual.idUsuario)
					return -1;
				else if(idUsuario>actual.idUsuario)
					return 1;
				else return 0;
			}
		}
	}
	
}
