package model.data_structures;

import java.security.NoSuchAlgorithmException;

import model.data_structures.IQueue;

public class Queue<T> implements IQueue<T>{
	
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------
	
	private DoubleLinkedList<T> lista;
	
	// -----------------------------------------------------------------
	// Constructor
	// -----------------------------------------------------------------
	
	public Queue(){
		lista = new DoubleLinkedList<T>();
	}

	public void enQueue(T item) {
		lista.add(item);
	}

	public T deQueue() {
		return lista.removeFirst();
	}

	public boolean isEmpty() {
		return lista.size()==0;
	}

	public int size() {
		return lista.size();
	}

}
