package model.data_structures;

public interface IStack<T> {
	
	/**
	 * Agrega un item al tope de la pila.
	 * @param item a agregar.
	 */
	public void push(T item);
	
	/**
	 * Elimina el elemento en el tope de la pila.
	 * @return Elemento eliminado.
	 */
	public T pop();
	
	/**
	 * Indica si la pila est� vac�a.
	 * @return true si est� vac�o, false de lo contrario.
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la pila
	 * @return N�mero de elementos.
	 */
	public int size();
}
