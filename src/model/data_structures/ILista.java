package model.data_structures;

public interface ILista<T> extends Iterable<T>{
	
	/**
	 * Agrega elemento al final.
	 * @param elem
	 */
	public void add(T elem);
	
	/**
	 * Agrega elemento al principio
	 * @param e
	 */
	public void addFirst(T e);
	
	/**
	 * Agregar elemento en una posici�n espec�fica.
	 */
	public boolean set(T e, int pos);
	
	/**
	 * Retorna el elemento en una posici�n dada
	 * @param pos
	 * @return
	 */
	public T get(int pos);
	
	/**
	 * Elimina el elemento de la posici�n dada.
	 * @param pos
	 * @return
	 */
	public T remove(int pos);
	
	/**
	 * Retorna el tama�o de la lista.
	 * @return
	 */
	public int size();
	
	/**
	 * Elimina el primer elemento de la lista
	 * @return
	 */
	public T removeFirst();
	
	/**
	 * Elimina el �ltimo elemento de la lista
	 * @return
	 */
	public T removeLast();
	
	/**
	 * Indica si la lista est� vac�a o no
	 * @return
	 */
	public boolean isEmpty();


}
