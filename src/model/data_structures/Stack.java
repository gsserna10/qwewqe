package model.data_structures;


public class Stack<T> implements IStack<T>{
	
	private ListaEncadenada<T> pila;

	public Stack(){
		pila = new ListaEncadenada<T>();
	}
	
	public void push(T item) {
		pila.addFirst(item);
	}

	public T pop() {
		return pila.removeFirst();
	}

	public boolean isEmpty() {
		return pila.size() == 0;
	}

	public int size() {
		return pila.size();
	}

}
